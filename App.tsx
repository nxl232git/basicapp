import React from 'react';

//components
import { Provider } from 'react-redux'
import Navigator from './src/navigation/navigator';
import store from './src/redux/store/store';


const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};
export default App;


