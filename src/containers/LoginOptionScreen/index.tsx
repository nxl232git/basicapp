import React, { useState, createRef, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity} from 'react-native'


//components
import ButtonSuccess from '../../components/ButtonSuccess'

import styles from './style';
import images from '../../assets/images';
import { SafeAreaView } from 'react-native-safe-area-context';

const LoginOptionScreen = ({navigation}) => {
     return (
          <SafeAreaView style={styles.container}>
               <View style={styles.logo_Top}>
                    <Image source={images.logo_red} style={styles.image_logo_style} />
               </View>
               <View style={styles.content_Center}>
                    <Text style={styles.title_style}>TODO LIST EVERY THING</Text>
                    <Text style={styles.contents_style}>
                         Làm tất cả mọi thứ có thể!
                    </Text>
               </View>
               <View style={styles.button_Bottom}>
                    <ButtonSuccess
                         title={'Login'}
                         style={{
                              containerStyle: styles.button_Success_login, 
                              titleStyle: styles.title_button_style_Login,
                         }}
                         HaveImage={false}
                         onPress={()=> navigation.navigate('Login')}     
                    />
                    <ButtonSuccess
                         title={'Sign Up with Google'}
                         style={{
                              containerStyle: styles.button_Success_signUp, 
                              titleStyle: styles.title_button_style_signUp,
                         }}
                         HaveImage={true}
                         image={images.Google_logo}
                         // onPress={()=> navigation.navigate('Login')}     
                    />
                    <View style={styles.SignUp_btn_View}>
                         <Text style={styles.title_have_an_Acount}>Already have an Acount?</Text>
                         <TouchableOpacity style={styles.Button_Signup}>
                              <Text style={styles.title_Button_SignUp}>Sign Up</Text>
                         </TouchableOpacity>
                    </View>
                         
               </View>
          </SafeAreaView>
     )
}
export default LoginOptionScreen;
