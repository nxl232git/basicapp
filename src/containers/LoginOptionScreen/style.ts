import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';

const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;


const styles = StyleSheet.create({
     container: {
          height: Heights,
          width: Widths,
     },
     logo_Top: {
          width: Widths,
          height: '38%',
          flexDirection: 'column-reverse'

     },
     image_logo_style: {
          width: Widths / 3,
          height: Widths / 2.98,
          alignSelf: 'center',
          justifyContent: 'flex-end',
          marginBottom: Heights / 17
     },
     content_Center: {
          width: Widths,
          height: '22%',
     },
     title_style: {
          alignSelf: 'center',
          fontSize: 30,
          fontWeight: 'bold',

     },
     contents_style: {
          alignContent: 'center',
          alignSelf: 'center',

     },
     button_Bottom: {
          width: Widths,
          height: '40%',
     },
     button_Success_login: {
          height: Heights / 13,
          width: '85%',
          backgroundColor: Palette.red,
     },
     title_button_style_Login: {
          color: Palette.white,
          fontSize: 24,
          fontWeight: 'bold',
     },
     button_Success_signUp: {
          height: Heights / 13,
          width: '85%',
          marginVertical: Heights/40,
          borderWidth: 1,
          borderColor: Palette.grey
     },
     title_button_style_signUp: {
          color: Palette.grey,
          fontSize: 22,
          fontWeight: 'bold',
     },
     SignUp_btn_View:{
          width: '85%',
          alignSelf:'center',
          flexDirection:'row',
          justifyContent:'center'
     },
     title_have_an_Acount:{
          fontSize: 15
     },
     Button_Signup:{
          marginLeft: 9
     },
     title_Button_SignUp:{
          fontSize: 15,
          color: Palette.red
     }

})
export default styles;
