import { NavigationContainer } from '@react-navigation/native';
import React, { useState, createRef, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, Alert, TextInput } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import images from '../../assets/images';

//Components
import ButtonBack from '../../components/ButtonBack'
import ButtonSuccess from '../../components/ButtonSuccess';
import Inputs from '../../components/Inputs'
import styles from './style';

const InputScreen = ({ }) => {
    const [index, setIndex] = useState("RUSH");
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.Top_Background}>
                <View style={styles.profilio}>
                    <View style={styles.Profile} >
                        <Image source={images.Profile} style={styles.img_profile} />
                    </View>
                    <View style={styles.Text_Profile}>
                        <Text style={styles.Font}>Nguyễn Xuân Long</Text>
                        <Text style={styles.feel}>Thời tiết hôm nay thật đẹp!</Text>
                    </View>
                </View>
            </View>
            <View style={styles.View_main}>
                <View style={styles.view_Inputs}>
                    <Inputs
                        placeHolder={'Nhập vào tiêu đề...'}
                    />
                    <Inputs
                        placeHolder={'Nhập vào nội dung công việc...'}
                        Multiline={true}
                    />
                    <View style={styles.level_work}>
                        <TouchableOpacity onPress={() => setIndex("RUSH")} style={[styles.button_level, { backgroundColor: index == "RUSH" ? 'red' : '#fff' }]}>
                            <Text style={styles.title_style}>Quan trọng</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setIndex("NEED")} style={[styles.button_level, { backgroundColor: index == "NEED" ? 'yellow' : '#fff' }]}>
                            <Text style={styles.title_style}>Cần thiết</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setIndex("NORM")} style={[styles.button_level, { backgroundColor: index == "NORM" ? 'green' : '#fff' }]}>
                            <Text style={styles.title_style}>Bình thường</Text>
                        </TouchableOpacity>
                    </View>
                    <ButtonSuccess
                        title={'SAVE'}
                        style={{
                            containerStyle: styles.button_Success,
                            titleStyle: styles.title_style_save,
                        }}
                        HaveImage={false}

                    />
                </View>
            </View>
        </SafeAreaView>
    )
}
export default InputScreen;
