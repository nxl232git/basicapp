import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Top_Background:{
        width: '100%',
        height: Heights/4.59,
        alignSelf:'center',
    },
    profilio:{
        width: Widths - Widths * 0.1,
        height: Heights/6,
        borderRadius: 10,
        alignSelf: 'center',
        marginTop: 20,
        backgroundColor:Palette.red,
        elevation: 10,
        shadowOffset: {
            width: 8,
            height: 8,
        },
        flexDirection: 'row'
    },
    Profile: {
        width: Widths / 5,
        height: Widths / 5,
        borderRadius: 20,
        marginLeft: 15,
        alignSelf: 'center',
    },
    View_main:{
        width:'90%',
        justifyContent:'center',
        alignSelf:'center',
        elevation: 10,
        shadowOffset: {
            width: 8,
            height: 8,
        },
        backgroundColor:Palette.white,
        height: Heights/2,
        borderRadius: 8

    },
    view_Inputs:{
        width:'90%',
        justifyContent:'center',
        alignSelf:'center',
        fontSize: 18,
        
    },
    input_style:{
        width: '100%',
        height: Heights/3,
        borderWidth: 1,
        borderColor: Palette.grey,
        marginTop: Heights/20,
        borderRadius: 6,

    },
    text_input_Style:{
        width: '100%',
        height: '100%',
    },
    level_work:{
        width: '100%',
        marginTop: Heights/18,
        justifyContent:'space-between',
        flexDirection:'row'
    },
    button_level:{
        width: Widths/4,
        height: Heights/16,
        backgroundColor: 'green',
        borderRadius: 6,
        justifyContent:'center'
    },
    title_style :{
        color: Palette.black,
        alignSelf:'center',

    },
    button_Success:{
        width: '100%',
        height: Heights/14,
        justifyContent:'center',
        backgroundColor: Palette.red,
        marginTop: 20

    },
    title_style_save:{
        color: Palette.white,
        fontSize: 25,
        fontWeight: 'bold'
    },
    Text_Profile:{
        alignSelf: 'center',
        marginLeft:20
    },
    Font:{
        fontSize: 20,
        color: Palette.white
    },
    img_profile:{
        width: Widths/5,
        height: Widths / 5,
    },
    feel:{
        color: Palette.black
    },
    

})
export default styles;
