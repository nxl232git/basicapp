import { NavigationContainer } from '@react-navigation/native';
import React, { useState, createRef, useEffect, useCallback } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, Alert, TextInput } from 'react-native'

//Components
import ButtonBack from '../../components/ButtonBack'
import ButtonSuccess from '../../components/ButtonSuccess';
import Inputs from '../../components/Inputs'
import styles from './style'; 

//redux-sagas
import { LoginRequested } from '../../redux/actions/Login.act';
import { useDispatch } from "react-redux";
import { SafeAreaView } from 'react-native-safe-area-context';

const LoginScreen = ({ navigation }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
  
    const Submit = () => {
      dispatch(LoginRequested(username, password));
    };
  
    const handleChangeUserName = useCallback((text: any) => {
      setUsername(text);
    }, []);
  
    const handleChangePassword = useCallback((text: any) => {
      setPassword(text);
    }, []);
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.Button_Top_View}>
                <ButtonBack onPress={() => navigation.goBack('')} />
                <TouchableOpacity style={styles.Button_Forgot_PassWord}>
                    <Text style={styles.text_Button_Forgot} >Forgot Password?</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.Title_Top_View}>
                <Text style={styles.title_Login}>ĐĂNG NHẬP</Text>
            </View>
            <View style={styles.inputs}>
                <Inputs
                    title_Input={"Email"}
                    keyboardType={'email-address'}
                    placeHolder={'longnx.bhsoft@gmail.com'}
                    surcureTextEntry={false}
                    onChangeText={handleChangeUserName}
                />
                <Inputs
                    title_Input={"Password"}
                    surcureTextEntry={true}
                    keyboardType={'default'}
                    placeHolder={'Mật khẩu'}
                    onChangeText={handleChangePassword}
                />

            </View>
            <ButtonSuccess
                title={'Continue'}
                style={{
                    containerStyle: styles.button_Success,
                    titleStyle: styles.title_style,
                }}
                HaveImage={false}
                onPress={Submit}
                // onPress={()=> navigation.replace('Home')}
            />

        </SafeAreaView>
    )
}
export default LoginScreen;
