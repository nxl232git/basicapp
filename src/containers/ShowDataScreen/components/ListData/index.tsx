import React, { useState, useEffect } from 'react'
import { View, Text, Image, TouchableOpacity, ImageBackground, FlatList } from 'react-native'
import images from '../../../../assets/images'
import { Palette } from '../../../../themes/colors'
import styles from './style';
import Swipeout from 'react-native-swipeout';
import Checkbox from '../CheckBox'

interface Props {
    style?: Object,
    data?: FoodItemProps[],
    onPress1?: () => void,
    onPress2?: () => void,
    onPress3?: () => void,
    stt?: number,
    checked: boolean,
    title: string,
    id: number
}


interface FoodItemProps {
    
}
const ListData = (props: Props) => {
    const [color, setColor] = useState('green')
    useEffect(() => {
        if(props.stt == 0){
            setColor('green')
        } if (props.stt == 1) {
            setColor('yellow')
        } if(props.stt == 2) {
            setColor('red')
        }
    })
    return (
        <Swipeout
            autoClose={true}
            style={styles.swipeout}
        >
            <TouchableOpacity style={[styles.container]} onPress={props.onPress2}>
                <View style={[styles.status, {backgroundColor: color}]}></View>
                <View style={styles.title_view}>
                   <Checkbox style={styles.checkbox} checked={props.checked} />
                    <Text style={styles.title}>{props.title}</Text>
                </View>
            </TouchableOpacity>
        </Swipeout>
    )
}
export default ListData;