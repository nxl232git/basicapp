import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        width: "100%",
    },
    swipeout:{
        marginVertical: 10,
        width: Widths - Widths * 0.1,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 10,
        elevation: 7,
        shadowOffset: {
            width: 8,
            height: 8,
        },
        
    },
    status: {
        width: "5%",
        height: "100%",
        
    },
    title_view:{
        width: "80%",
        height: "100%",
        flexDirection: 'row',
        paddingLeft: 10,
        marginTop:5,
        marginBottom: 5,
        alignSelf:'center'
    },
    FlatlistFB:{
        
    },
    title:{
        alignSelf: 'center',
        fontSize: 22,
        color: Palette.grey,
        justifyContent:'center'
    },
    checkbox:{
        alignSelf:'center'
    }
    
})
export default styles;
