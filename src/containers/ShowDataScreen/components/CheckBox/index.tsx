import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import styles from './style'
import images from '../../../../assets/images';

interface Props{
    style?: Object,
    checked: boolean,
}

const CheckBox = (props:Props) => {
    return (
        <TouchableOpacity style={[styles.container,props.style]} >
            <Image source={props.checked ? images.iconCheck : images.iconUnCheck} style={styles.imgCheck} />
        </TouchableOpacity>
    );
};



export default CheckBox;
