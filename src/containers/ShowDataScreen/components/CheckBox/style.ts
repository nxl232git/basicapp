
import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width:40,
        height:40,
    },
    imgCheck: {
        width: 20,
        height: 20,
        resizeMode:'contain'
    }

});
export default styles;
