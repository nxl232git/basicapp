import { NavigationContainer } from '@react-navigation/native';
import React, { useState, createRef, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, Alert, TextInput, FlatList } from 'react-native'
import images from '../../assets/images';

//Components
import ListData from './components/ListData';
import styles from './style';
import Data from '../../DataTest/Data'
import { SafeAreaView } from 'react-native-safe-area-context';

const ShowDataScreen = () => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.Top_Background}>
                <View style={styles.profilio}>
                    <View style={styles.Profile} >
                        <Image source={images.Profile} style={styles.img_profile} />
                    </View>
                    <View style={styles.Text_Profile}>
                        <Text style={styles.Font}>Nguyễn Xuân Long</Text>
                        <Text style={styles.feel}>Thời tiết hôm nay thật đẹp!</Text>
                    </View>
                </View>
            </View>
            <View style={styles.Flatlist_view}>
                <FlatList
                    style={styles.Flatlist}
                    data={Data}
                    renderItem={({ item }) => {
                        return (
                            <ListData
                                id={item.id}
                                stt={item.stt}
                                checked={item.checked}
                                title={item.title}
                            />
                        );
                    }}
                    refreshing={true}
                    scrollsToTop={true}
                />
            </View>
        </SafeAreaView>
    )
}
export default ShowDataScreen;
