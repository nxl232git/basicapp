import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    Top_Background:{
        width: '100%',
        height: Heights/4.59,
        alignSelf:'center',
    },
    profilio:{
        width: Widths - Widths * 0.1,
        height: Heights/6,
        borderRadius: 10,
        alignSelf: 'center',
        marginTop: 20,
        backgroundColor:Palette.red,
        elevation: 10,
        shadowOffset: {
            width: 8,
            height: 8,
        },
        flexDirection: 'row'
    },
    Profile: {
        width: Widths / 5,
        height: Widths / 5,
        borderRadius: 20,
        marginLeft: 15,
        alignSelf: 'center',
    },
    Flatlist:{
        height: Heights
    },
    Flatlist_view:{
        width: '100%',
        height: Heights,
    },
    Text_Profile:{
        alignSelf: 'center',
        marginLeft:20
    },
    Font:{
        fontSize: 20,
        color: Palette.white
    },
    img_profile:{
        width: Widths/5,
        height: Widths / 5,
    },
    feel:{
        color: Palette.black
    },

})
export default styles;
