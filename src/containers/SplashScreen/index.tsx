import React, { useState, useEffect } from 'react';
import {
     ActivityIndicator,
     View,
     Image,
     Text,
     ImageBackground
} from 'react-native';


import styles from './style'
import Loader from '../../components/Loader'
import images from '../../assets/images';
import {useSelector} from 'react-redux';

const SplashScreen = ({ navigation }) => {
     const [animating, setAnimating] = useState(true);
     const [loading, setLoading] = useState(false);
     // const isAuthenticated = useSelector(
     //      (state: any) => state.Home.isAuthenticated,
     // );
     useEffect(() => {
          setTimeout(() => {
               setLoading(true);
               setAnimating(true);
               navigation.replace('LoginOption')
               //isAuthenticated ? (navigation.replace('LoginOption')) : (navigation.raplace('Home'));
               setLoading(false);
          }, 3000);
     }, []);

     return (
          <View style={styles.container}>
               <Image
                    source={images.logo}
                    style={styles.image_background}
               />
               <Text style={styles.title_splash}>TODO LIST</Text>
               <ActivityIndicator
                    animating={animating}
                    color="#FFFFFF"
                    size="large"
                    style={styles.activity_Indicator}
               />
          </View>
     );
};

export default SplashScreen;