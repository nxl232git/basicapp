const images = {
    logo:                       require('./image/drawable-mdpi/logo.png'),
    logo_red:                   require('./image/drawable-mdpi/logo-red.png'),
    Google_logo:                require('./image/drawable-mdpi/GoogleLogo.png'),
    arrow_back:                 require('./image/drawable-mdpi/arrow_back.png'),
    icon_Input:                 require('./image/drawable-mdpi/input.png'),
    icon_Database:              require('./image/drawable-mdpi/database.png'),
    icon_setting:               require('./image/drawable-mdpi/settings.png'),
    icon_show:                  require('./image/drawable-mdpi/show.png'),
    BackGround_Gradient:        require('./image/drawable-mdpi/backGround.png'),
    iconCheck:                  require('./image/drawable-mdpi/icCheckMark.png'),
    iconUnCheck:                require('./image/drawable-mdpi/icUnCheckMark.png'),
    Feedback:                   require('./image/drawable-mdpi/feedback.png'),
    Version:                    require('./image/drawable-mdpi/history.png'),
    Logout:                     require('./image/drawable-mdpi/logout.png'),
    Profile:                    require('./image/drawable-mdpi/profile.png'),
};
export default images;
