import React from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Palette } from '../../themes/colors';
const Widths = Dimensions.get('window').width;
const Heights = Dimensions.get('window').height;
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: Heights/15,
        borderBottomWidth: 1,
        borderBottomColor: Palette.grey,
        alignSelf:'center'
    },
    title_Email:{
        color: Palette.grey,
        marginBottom: 5

    },
    text_Input_style:{
        fontWeight: 'bold',
        fontSize: 15,
        paddingBottom:'2%',
    }

})
export default styles;
