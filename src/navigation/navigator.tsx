import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';

import SplashScreen from '../containers/SplashScreen';
import LoginOptionScreen from '../containers/LoginOptionScreen';
import LoginScreen from '../containers/LoginScreen';
import SettingScreen from '../containers/SettingScreen';
import AsyncStorage from '@react-native-community/async-storage';
import ButtonTab from '../components/BottomTab'

const Navigator = () => {
  const [login, setLogin] = useState('initialState')

  const isAuthenticated = useSelector(
    (state: any) => state.Home.isAuthenticated,
  );

  const Stack = createStackNavigator();

  useEffect(() => {
    AsyncStorage.getItem('token').then(
      (data) => setLogin(data),
    );
  }, [])
  
  console.log('Login status:', login)

  return (
    <NavigationContainer>
      {isAuthenticated ? (
        <Stack.Navigator>
          <Stack.Screen
            options={{ headerShown: false }}
            name="Splash"
            component={SplashScreen}
          />
          <Stack.Screen
            options={{ headerShown: false }}
            name="LoginOption"
            component={LoginOptionScreen}
          />
          <Stack.Screen
            options={{ headerShown: false }}
            name="Login"
            component={LoginScreen}
          />
        </Stack.Navigator>
      ) : (
          <Stack.Navigator>
            <Stack.Screen
              name="Home"
              options={{ headerShown: false }}
              component={ButtonTab}
            />
            <Stack.Screen
              name="Setting"
              options={{ headerShown: false }}
              component={SettingScreen}
            />
          </Stack.Navigator>
        )}
    </NavigationContainer>
  );
};
export default Navigator;
