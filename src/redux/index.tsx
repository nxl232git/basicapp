import {combineReducers} from 'redux'
import {REDUX_KEY as GlobalKey,reducer as GlobalReducer} from './GlobalRedux'


export default  combineReducers({
    [GlobalKey]: GlobalReducer
})