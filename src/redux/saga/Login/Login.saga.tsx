import { Alert } from 'react-native';
import { takeLatest, call, put } from 'redux-saga/effects';

import { LoginSuccessed, LoginFailed } from '../../actions/Login.act';
import * as types from '../../constants/login.const';
import * as RootNavigation from '../../../navigation/rootNavigation';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export function* loginSaga(action: any) {
  const getUrl = 'https://moki.com.vn/api/login';
  try {
    const data = yield call(axios.request, {
      url: getUrl,
      method: 'POST',
      data: {
        email_or_phone: action.data.username,
        password: action.data.password,
      },
    });
    
    console.log(data.data.status)
    if (data.data.status == 'success') {
      AsyncStorage.setItem('token', data.data.token);
      yield put(LoginSuccessed('6'));
    }
    else{
      Alert.alert('Đăng nhập thất bại')
    }
  } catch (error) {
    yield put(LoginFailed(error));
    
  }
}

export default function* signInSaga() {
  yield takeLatest(types.LOGIN_REQUESTED, loginSaga);
}