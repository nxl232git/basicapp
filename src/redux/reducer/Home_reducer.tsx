import produce from 'immer';

import * as types from '../constants/home.const';
import {LOGIN_SUCCESSED} from '../constants/login.const';
import AsyncStorage from '@react-native-community/async-storage';

interface Props {
  isAuthenticated: boolean;
}

const initialState: Props = {
  isAuthenticated: true,
};

export const Home_red = (state = initialState, action: any, ) =>
  produce(state, (draft) => {
    switch (action.type) {
      case types.LOGOUT:
        AsyncStorage.removeItem('token');
        draft.isAuthenticated = true;
        break;

      case LOGIN_SUCCESSED:
        draft.isAuthenticated = false;
        break;
        
      default:
        return state;
    }
  });
