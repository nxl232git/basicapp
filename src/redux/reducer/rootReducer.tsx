import {combineReducers} from 'redux';

import {loginReducer} from './Login_reducer';
import {Home_red} from './Home_reducer';

export default combineReducers({
  Login: loginReducer,
  Home: Home_red,
});
