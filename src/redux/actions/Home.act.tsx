import * as types from '../constants/home.const';

export const LogOut = () => {
  return {
    type: types.LOGOUT,
  };
};
